require('dotenv/config');
const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ServiceWorkerPlugin = require('serviceworker-webpack-plugin');
const BabiliPlugin = require('babili-webpack-plugin');

const { NODE_ENV, DEBUG } = process.env;

module.exports = ({
  prod = NODE_ENV === 'production',
  minify = false,
  debug = false,
  hot = false,
  lint = false,
  target = 'web'
} = {}) => {
  const isCordova = target.includes('cordova')
  console.log(JSON.stringify({ target, isCordova }, null, 2))

  const rules = [
    {
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      options: {
        cacheDirectory: true,
      },
    },
    {
      test: /\.(eot|ttf|woff2?|svgz?)$/,
      loader: 'file-loader',
      options: {
        name: 'fonts/[name].[ext]',
      },
    },
    {
      test: /\.(gif|png|jpe?g|webp)$/,
      loader: 'file-loader',
      options: {
        name: prod
        ? '[name]-[hash].[ext]'
        : '[name].[ext]',
      },
    },
    {
      test: /\.scss?$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          {
            loader: 'css-loader',
            options: {
              localIdentName: '[path][name]--[local]',
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer(),
              ],
            },
          },
          {
            loader: 'sass-loader',
          },
        ],
      }),
    },
  ];

  const plugins = [
    new CleanPlugin(['dist'], {
      // dry: !prod,
    }),
    new CopyPlugin([
      { from: 'app/favicon.ico' },
      { from: 'app/sound_a.mp3' },
      { from: 'assets', to: 'assets' },
      { from: 'app/emoji.json' },
      { from: 'app/manifest.json' },
      { from: 'app/images', to: 'images' },
    ]),
    new ExtractTextPlugin({
      filename: prod
      ? 'styles.bundle.[contenthash].css'
      : 'styles.bundle.css',
      allChunks: true,
      disable: !prod,
    }),
    new HtmlPlugin({
      isCordova,
      template: 'app/index.html',
      inject: 'body',
    }),
    new ServiceWorkerPlugin({
      entry: './app/service-worker/index.js',
      filename: 'service-worker.js',
      includes: prod
      ? ['**/*']
      : [],
      template: serviceWorkerOption => Promise.resolve(`
        var process = {
          env: ${JSON.stringify({
            NODE_ENV,
            DEBUG,
            TARGET: target
          })}
        };
      `),
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV,
      DEBUG: debug,
      TARGET: target,
    }),
  ];

  const config = {
    plugins,
    module: { rules },
    cache: false,
    devtool: prod
    ? 'source-map'
    : 'eval-inline-source-map',
    entry: {
      app: ['./app/index.jsx'],
    },
    output: {
      path: path.resolve('./dist'),
      publicPath: !isCordova
        ? '/'
        : target === 'cordova:ios'
          ? './'
          : '/android_asset/www',
      filename: prod && !isCordova
      ? '[name].bundle.[hash].js'
      : '[name].bundle.js',
      chunkFilename: prod && !isCordova
      ? '[id].chunk.[chunkhash].js'
      : '[id].chunk.js',
    },
    resolve: {
      extensions: ['.jsx', '.js', '.json'],
      modules: [
        path.resolve('./app'),
        path.resolve('./node_modules'),
      ],
    },
  };

  // switch (target) {
  //   case 'web':
  //   case 'cordova':
  // }

  if (lint) {
    // rules.unshift({
    //   test: /\.jsx?$/,
    //   enforce: 'pre',
    //   loader: 'eslint-loader',
    //   exclude: /node_modules/,
    //   options: {
    //     quiet: true,
    //   }
    // })
  }

  if (hot) {
    config.entry.app.unshift(
      'webpack-hot-middleware/client'
    );
    plugins.push(
      new webpack.HotModuleReplacementPlugin()
    );
  }

  if (minify) {
    plugins.push(
      // new webpack.optimize.UglifyJsPlugin({
      //   sourceMap: true,
      //   comments: false,
      //   mangle: true,
      //   compress: {
      //     warnings: false,
      //     drop_console: false,
      //   },
      // }),
      new BabiliPlugin({
        sourceMap: true
      }),
      new CompressionPlugin({
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.(css|js|html)$/,
        threshold: 1240,
        minRatio: 0.8,
      })
    );
  }

  return config;
};
