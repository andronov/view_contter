/*import mqtt from 'mqtt';
import { MQTT_SERVER__URL, FAVICON_NEW_BASE64 } from './constants';
import Message from 'stores/Message';
import ThreadUser from 'stores/ThreadUser';
import chipMessage from 'stores/ChipMessage';
import Thread from 'stores/Thread';
import TypingUser from 'stores/TypingUser';
import AuthStorage from './AuthStorage';
import appState from 'stores/AppState';
import { blinkTitle, blinkTitleStop, playSoundNotify } from 'services/utils';
import moment from 'moment';
let mqttClient = null;
let attemps = 0;
import uuid from 'uuid';

export function createConnection() {
  const jwt = AuthStorage.getSession().jwt;
  let c_date = null;
  const lookupWorker = {};
  let timer = null;

  const opts = {
    username: jwt,
    password: jwt,
    clientId: uuid.v4(),
  };

  mqttClient = mqtt.connect(MQTT_SERVER__URL, opts);

  mqttClient.on('connect', () => {
    console.log('-----connected', jwt);
    c_date = new Date();
    attemps = 0;
    mqttClient.subscribe(`private/${jwt}/chat`);
  });

  mqttClient.on('message', (topic, dt) => {
    if (new Date() - c_date < 3200) { return; }
    const payload = JSON.parse(dt.toString());
    const dataload = JSON.parse(dt.toString());
    console.log('----message', payload, payload.BrowserTabId, window.BrowserTabId);

    const favicon = document.getElementById('favicon');
    if (payload.action == 'message_typing') {
      let us = null,
        thread = null;
      appState.currentUser.userThreads.slice().filter(th => { return th.thread.threadUsers.slice().filter(t => { if (t.user.id == payload.data.user.id) { us = t.user; } }); });
      appState.currentUser.userThreads.filter(th => { if (th.thread.id == payload.data.threadId) { thread = th.thread; } });
      if (appState.currentUser.id == us.id) { return; }
      if (!thread.typingUsers.filter(it => { return it.user.id == us.id; }).length) {
        thread.typingUsers.concat(TypingUser.create({ user: us, thread, created: moment() }));
      }			else {
        thread.typingUsers.filter(it => { return it.user.id == us.id; })[0].created = moment();
      }
      thread.typingUpdate();
      setTimeout(() => { thread.typingUpdate(); }, 3000);
    }

    if (payload.BrowserTabId && window.BrowserTabId && (window.BrowserTabId == payload.BrowserTabId)) {
      return;
    }

    let messageData = null,
      newMessage = null;
    if (payload.action == 'message_add' || payload.editMessage) {
      messageData = payload.editMessage ? payload.editMessage.message : payload.createMessage.message;
      const idl = messageData.id;
      if (payload.editMessage) { clearTimeout(lookupWorker[idl]); }
      console.log('from websockets', messageData);
      timer = payload.editMessage ? 3000 : 0;

      if (payload.createMessage) {
        if (!messageData.parent) {
          messageData.parent = appState.currentUser.userThreads.filter(ut => { return ut.thread.id === messageData.recipient; })[0].thread.rootMessage;
        }
        newMessage = Message.create({ ...messageData, position: !messageData.position ? messageData.created : messageData.position });
        if (newMessage.recipient.id != appState.currentUser.selectedThreadId) {
          newMessage.recipient.newMessage = true;
        }
        newMessage.recipient.updateScroll();
      }			else {
        lookupWorker[idl] = setTimeout(() => {
          if (!messageData.parent) {
            messageData.parent = appState.currentUser.userThreads.filter(ut => { return ut.thread.id === messageData.recipient; })[0].thread.rootMessage;
          }
          newMessage = Message.create({
            ...messageData,
            position: !messageData.position ? messageData.created : messageData.position,
          });
          if (newMessage.recipient.id != appState.currentUser.selectedThreadId) {
            newMessage.recipient.newMessage = true;
          }
          newMessage.recipient.updateScroll();
        }, timer);
      }
    }
    if (payload.createThread || payload.editThread) {
      let newThreadData = payload.createThread ? payload.createThread.thread : payload.editThread.thread;
      const threadRecord = newThreadData;
      const members = threadRecord.members;
      delete threadRecord.members;
      const newThread = Thread.create(newThreadData);
      members.concat(newThread.creator).forEach(user => {
        ThreadUser.create({ thread: newThread, user, id: `${newThread.id}_${user.id}` });
      });
    }
    if (payload.createChipMessage) {
      payload.createChipMessage.chipMessage.chipValue = payload.createChipMessage.chipMessage.value;
      chipMessage.create(payload.createChipMessage.chipMessage);
    }
    if (payload.editChipMessage) {
      clearTimeout(lookupWorker[payload.editChipMessage.chipMessage.id]);
      lookupWorker[payload.editChipMessage.chipMessage.id] = setTimeout(() => {
        payload.editChipMessage.chipMessage.chipValue = payload.editChipMessage.chipMessage.value;
        chipMessage.create(payload.editChipMessage.chipMessage);
      }, 3000);
    }
    if (payload.deleteChipMessage) {
      payload.deleteChipMessage.chipMessage.chipValue = payload.deleteChipMessage.chipMessage.value;
      const c_msg = chipMessage.create(payload.deleteChipMessage.chipMessage);
      c_msg.delete();
    }

    if (payload.deleteMessage) {
      appState.currentUser.userThreads.slice().forEach(th => {
        th.thread.messages.filter(msg => { return msg.id == payload.deleteMessage.message.id; }).slice().forEach(ms => {
          ms.delete();
        });
      });
    }

    if (newMessage && payload.createMessage) { dataload.threadName = newMessage.recipient.threadName; }
    if (payload.createMessage && newMessage && (newMessage.text.length || newMessage.file.length)) {
    	if (appState.currentUser.id == newMessage.sender.id) { return; }
      if (document.visibilityState == 'hidden') {
        blinkTitleStop();
        const news = appState.currentUser.userThreads.filter(th => { return th.thread.newMessage; });
        let len = news.length;
        if (!window.documentTitle) { window.documentTitle = document.title; }
        playSoundNotify();
        if (newMessage.recipient.id == appState.currentUser.selectedThreadId) { len++; }
        blinkTitle(document.title, `${len} new notification${news.length >= 1 ? 's' : ''}`, 800);
        favicon.href = FAVICON_NEW_BASE64;
      }
      navigator.serviceWorker.getRegistration().then((registration) => {
        registration.getNotifications().then((notifications) => {
          notifications.forEach((notification) => {
            notification.close();
          });
        });
      });

      navigator.serviceWorker.controller ? navigator.serviceWorker.controller.postMessage({
        action: 'sendDataServiceWorker',
        currentUser: appState.currentUser,
        payload: JSON.stringify(dataload),
        documentUrl: `${location.protocol}//${location.host}`,
      }) : null;
    }
  });
}


export function sendTyping(data) {
  mqttClient.publish('private/chat', JSON.stringify(data));

}*/