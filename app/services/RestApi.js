import http from './http';
import AuthStorage from './AuthStorage';
import { BASE_URL } from './constants';

export function RestApi(data, url, method) {
  return http({
    method: method ? method : 'POST',
    url: `${BASE_URL}${url}`,
    data,
  })
}