// import 'react-hot-loader/patch'
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider, observer } from 'mobx-react';
import { AppContainer } from 'react-hot-loader'
import App from './components/App'
import { registerServiceWorker } from './services/ServiceWorkerRegistration';
import createBrowserHistory from 'history/createBrowserHistory'
import createMemoryHistory from 'history/createMemoryHistory'
import appState from './stores/AppState';
import {createBrowserTabId} from './services/utils'
import {createConnection} from './services/websockets';
import './styles/main.scss';
import './style.scss';
import isCordova from './lib/isCordova'
import { Auth} from './lib/react-jwt-auth';

const history = isCordova 
  ? createMemoryHistory()
  : createBrowserHistory();

import moment from 'moment';
window.moment = moment;

appState.router.setHistory(history);

//injectTapEventPlugin();

window.requestAnimFrame = (function(){
  return  window.requestAnimationFrame       ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame    ||
    window.oRequestAnimationFrame      ||
    window.msRequestAnimationFrame     ||
    function(/* function */ callback, /* DOMElement */ element){
      window.setTimeout(callback, 1000 / 60);
    };
})();

// TODO добавить кэширование в сервис воркер
//createBrowserTabId();
//registerServiceWorker();

document.addEventListener('deviceready', ev => {
  ReactDOM.render(
    <Auth baseUrl="http://127.0.0.1:8070/api/v1/">
    <AppContainer>
      <Provider appState={appState}>
          <App/>
      </Provider>
    </AppContainer>
    </Auth>,
    document.getElementById('root')
  );
}, false);

if (!isCordova) {
  document.dispatchEvent(new CustomEvent('deviceready'))
}

if (module.hot) {
  module.hot.accept()
}
