import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import User from './User';
import Item from './Item';

export default class Site extends BaseClass {
  static table = 'sites';

  static schema() {
    return {
      links: {
        type: 'many',
        inverse: 'links',
        factory: Item,
      }
    };
  }

  @observable id;
  @observable name;
  @observable description;
  @observable url;
  @observable short_url;
  @observable rss;
  @observable image;
  @observable favicon;
  @observable slug;
  @observable color;
  @observable followers;
  @observable lang;
  @observable type;
  @observable links = [];
}