import * as React from 'react';
import { observer, inject } from 'mobx-react';
import './style.scss';
import colorPalettes from 'color-palettes'
import { stickersG } from './demo';

import {Motion, spring} from 'react-motion';

import Draggable from 'react-draggable'; // The default
import {DraggableCore} from 'react-draggable'; // <DraggableCore>

let _id = 1;

@inject('appState')
@observer
export default class SharePage extends React.Component {

  state = {
    canvas: null,
    context: null,
    stickers: stickersG,
    objectsOpen: false,
    stickersOpen: false,
    elementsOpen: false,
    emojiOpen: false,
    objectsOpenHeaderEmoji: true,
    emojiCatSize: 'x3',
    emoji: [],
    emojiCat: [],
    items: []
  };

  componentDidMount() {
  }

  onLoadBasicImg = (e) => {
    let { target } = e;
    console.log('onLoadBasicImg', target);
    let cp = new colorPalettes(target.src);
    cp.dominantThree({
      format: 'hex'
    }).then( (result) => {
      console.log(result) // #
    })
  };

  onClickItem = (e, item) => {
    let items = this.state.items;
    let it = items;
    it.push({id: _id, item: item, is_active: true});
    this.setState({items: it});
    _id++;
  };

  loadEmoji = () => {
    // eslint-disable-next-line global-require, import/no-dynamic-require
    let res = require(`../../emoji.json`);
    console.log('loadEmoji', res);
    let categ = [],
      cat = null,
      categories = [],
      emoji = [],
      i =  1;
    for(let key in res){
      if(categ.indexOf(res[key].category) === -1){
        categ.push(res[key].category);
        categories.push({id: i, name: res[key].category, active: i === 1, emoji: []});
        i++;
      }
      res[key].path = `/assets/emoji/png_128x128/${res[key].unicode}.png`;
      emoji.push(res)
    }
    for(let key in res){
      cat = categories.filter(it=>it.name === res[key].category)[0];
      cat.emoji.push(res[key]);
      //console.log('loadEm3oqji', cat, res[key].category)
    }
    this.setState({emojiCat: categories, emoji: emoji});
    ///console.log('loadEmoqji', categories, emoji);
  };

  renderStickersList = () => {
    let { stickers } = this.state;
    return(
      <div className='objects__template'>
        <div className='objects__items'>
          {stickers.map(item=>{
            return(
              <div key={item.id} onClick={(e, it) => this.onClickItem(e, item)} className='objects__item'>
                <img width={150} src={item.path}/>
              </div>
            )
          })}
        </div>
      </div>
    )
  };

  renderEmojisList = () => {
    let { stickers, emojiCat, emojiCatSize } = this.state;
    let activeEmojiCat = emojiCat[1];
    return(
    <div className='objects__template'>
        {this.renderObjectsCat()}
        <div className='objects__items o_emoji'>
          {activeEmojiCat && activeEmojiCat.emoji.map(item => {
            return (
              <div key={item.unicode} 
              onClick={(e, it) => this.onClickItem(e, item)} 
              style={{width: ((emojiCatSize === 'x1' && '190px') || (emojiCatSize === 'x2' && '90px') || (emojiCatSize === 'x3' && '60px'))}}
              className='objects__item'>
                <img 
                style={{width: ((emojiCatSize === 'x1' && '190px') || (emojiCatSize === 'x2' && '90px') || (emojiCatSize === 'x3' && '60px'))}}
                src={`/assets/emoji/${emojiCatSize === 'x1' || emojiCatSize === 'x2' ? 'png_128x128' : 'png'}/${item.unicode}.png`} />
              </div>
            );
          })}
        </div>
      </div>
    )
  };

  renderObjectsCat = () => {
    let { emojiCat, emojiCatSize, objectsOpenHeaderEmoji } = this.state;

    return(
      <div>
        <Motion style={{x: spring(objectsOpenHeaderEmoji  ? 0 : -172)}}>
        {({x}) =>
        <div className="objects__header" style={{
                WebkitTransform: `translate3d(0, ${x}px, 0)`,
                transform: `translate3d(, ${x}px, 0)`,
              }}>
          <div className="objects__header_inner" >
            <div className="objects__header_size">
              <div className="objects__header_t">
                Size
              </div>
              <div className="objects__header_items">
                <div onClick={() => {this.setState({emojiCatSize: 'x1'})}} 
                className={`objects__header_item ${emojiCatSize === 'x1' ? 'active' : ''}`}>
                     x1
                </div>
                <div onClick={() => {this.setState({emojiCatSize: 'x2'})}}  
                className={`objects__header_item ${emojiCatSize === 'x2' ? 'active' : ''}`}>
                     x2
                </div>
                <div onClick={() => {this.setState({emojiCatSize: 'x3'})}}  
                className={`objects__header_item ${emojiCatSize === 'x3' ? 'active' : ''}`}>
                     x3
                </div>
              </div>
            </div>
            <div className="objects__header_cat">
              <div className="objects__header_t">
                Category
              </div>
              <div className="objects__header_items">
                <div className={`objects__header_item active`}>
                  recent
                </div>
                {emojiCat.map(item => {
                  return (
                    <div className='objects__header_item'>
                      {item.name}
                    </div>
                  );
                })}
                {/*emojiCat.map(item => {
                  return (
                    <div className='objects__header_item'>
                      <img src={`/assets/emoji/png/1f606.png`} />
                    </div>
                  );
                })*/}
              </div>
            </div>
          </div>

        <Motion style={{x: spring(objectsOpenHeaderEmoji  ? 0 : 180), y: spring(objectsOpenHeaderEmoji  ? 0 : 7), z: spring(objectsOpenHeaderEmoji  ? 7 : 0)}}>
        {({x, y, z}) =>
          <div onClick={() => {this.setState({objectsOpenHeaderEmoji: !this.state.objectsOpenHeaderEmoji})}}   
              className="objects__header_bottom">
          <div className="objects__header_bottom_arr" style={{
                WebkitTransform: `rotate(${x}deg)`,
                transform: `rotate(${x}deg)`,
                marginBottom: `${z}px`,
                marginTop: `${y}px`
              }}/>
          <div className="objects__header_bottom_arr" style={{
                WebkitTransform: `rotate(${x}deg)`,
                transform: `rotate(${x}deg)`,
                marginBottom: `${z}px`,
                marginTop: `${y}px`
              }}/>
          </div>
          }
        </Motion>

        </div>}
        </Motion>
      </div>
      )
  };

  renderObjectsSizeSlider = () => {
    return(
      <div className="o_slider">
        <div className="o_slider__inner">
          1
        </div>
      </div>
      )
  };

  onClickObjectsSticker = () => {
    this.setState({objectsOpen: !this.state.objectsOpen, stickersOpen: !this.state.stickersOpen, emojiOpen: false})
  };

  onClickObjectsElements = () => {
    this.setState({objectsOpen: !this.state.objectsOpen, elementsOpen: !this.state.elementsOpen,
      stickersOpen: false, emojiOpen: false})
  };

  onClickObjectsEmoji = () => {
    this.setState({objectsOpen: !this.state.objectsOpen, stickersOpen: false, emojiOpen: !this.state.emojiOpen});
    if(!this.state.emoji.length){
      this.loadEmoji()
    }
  };

  onClickObjects = (e, type) => {
    let { objectsOpen, emojiOpen, stickersOpen, elementsOpen} = this.state;
    let c = this.state[type];
    let dt = {emojiOpen: false, stickersOpen: false, elementsOpen: false};
    let res = !c;
    dt[type] = !c;
   // dt['objectsOpen'] = (!emojiOpen && !stickersOpen && !elementsOpen) ? true : false;
    this.setState({...dt, objectsOpen: !objectsOpen});

    if(res){
       this.setState({objectsOpen: true});
    }


    if(!this.state.emoji.length && type === 'emojiOpen'){
      this.loadEmoji()
    }
  };


  renderItems = () => {
    let { items } = this.state;

    return(
      <div className="object__layer">
        {items.map(item=>{
          return(
            <div key={item.id} className='item'>
              <Draggable>
                <div className='item__inner'>
                  <img width={150} src={item.item.path}/>
                </div>
              </Draggable>
            </div>
          )
        })}
      </div>
    )
  };

  renderElementsList = () => {
    return(
      <div className='objects__template'>
      elem
      </div>
    )
  };


  render() {
    const { objectsOpen, 
      elementsOpen, stickersOpen, emojiOpen } = this.state;


    return (
      <div className='share'>
        <div className='editor'>

          <div className='actions'>
            <div className='actions__inner'>
              <div className='actions__menu'>

                <div onClick={(e, it) => this.onClickObjects(e, 'templatesOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_templates'>
                  </div>
                  <div className='actions__btn_text'>
                    Templates
                  </div>
                </div>

                <div onClick={(e, it) => this.onClickObjects(e, 'elementsOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_elements'>
                  </div>
                  <div className='actions__btn_text'>
                    Elements
                  </div>
                </div>

                <div onClick={(e, it) => this.onClickObjects(e, 'effectsOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_effects'>
                  </div>
                  <div className='actions__btn_text'>
                    Effects
                  </div>
                </div>

                <div onClick={(e, it) => this.onClickObjects(e, 'mentionOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_mention'>
                  </div>
                  <div className='actions__btn_text'>
                    Mention
                  </div>
                </div>

                <div onClick={(e, it) => this.onClickObjects(e, 'stickersOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_sticker'>
                  </div>
                  <div className='actions__btn_text'>
                    Sticker {/*<span className='actions__btn_text_l'>gif</span>*/}
                  </div>
                </div>

                <div onClick={(e, it) => this.onClickObjects(e, 'emojiOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_emoji'>
                  </div>
                  <div className='actions__btn_text'>
                    Emoji
                  </div>
                </div>

                <div onClick={(e, it) => this.onClickObjects(e, 'drawOpen')} className='actions__menu_item'>
                  <div className='actions__btn actions__btn_draw'>
                  </div>
                  <div className='actions__btn_text'>
                    Draw
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div className='b_actions'>
            <div className='b_actions__inner'>
              <div className='b_actions__menu'>

                <div className='b_actions__menu_item'>
                  <div className='b_actions__btn b_actions__btn_trash'>
                  </div>
                  <div className='b_actions__btn_text'>
                    Trash
                  </div>
                </div>

              </div>
            </div>
          </div>



          <Motion style={{x: spring(objectsOpen ? 0 : -200)}}>
            {({x}) =>
              <div className='objects' style={{
                WebkitTransform: `translate3d(${x}px, 0, 0)`,
                transform: `translate3d(${x}px, 0, 0)`,
              }}>
                <div className='objects__inner'>
                  
                    {stickersOpen && this.renderStickersList()}
                    {emojiOpen && this.renderEmojisList()}
                    {elementsOpen && this.renderElementsList()}

                </div>
              </div>

            }
          </Motion>

         <div className="editor__inner">
           <div className="object">
             <div className="object__media">
               <img onLoad={this.onLoadBasicImg}
                    width={650}
                    className="object__img"
                    src="/assets/media/Looper-Movie-Poster-looper-32031468-2560-1920.jpg"/>
             </div>

             {this.renderItems()}


             <div className="object__block">
               <div className="object__block_info">
                 <a target="_blank" className="object__block_title" href="nytimes.com">Picks of the Month - May 2017</a>
                 <p target="_blank" className="object__block_desc" >May has come and gone. It was a very rich month with multiple non-technical resources. OK, some of you may call them "soft skills".</p>
               </div>
             </div>
             <div className="object__meta">
               <div className="object__meta_info">
                 <a target="_blank" href="nytimes.com">nytimes.com</a>
               </div>
             </div>
           </div>
         </div>
        </div>
      </div>
    );
  }
}