import React from 'react';
import { observer } from 'mobx-react';
import qs from 'qs';
import createMemoryHistory from 'history/createMemoryHistory';

const history = createMemoryHistory();

@observer
export default class Router extends React.Component {
  componentWillMount() {
    this._unlisten = history.listen(this.updateLocation);
    // window.addEventListener('popstate', this.updateLocation);
    this.updateLocation();
  }

  componentWillUnmount() {
    // window.removeEventListener('popstate', this.updateLocation);
    this._unlisten();
  }

  updateLocation = (location = history.location) => {
    const { pathname: path, search } = location;
    this.props.state.update({
      path,
      query: qs.parse(search.slice(1)),
    });
  };

  navigate(path = this.props.state.path, query) {
    history.push(path + qs.stringify(query));
    this.props.state.update({ query, path });
  }

  render() {
    const { children, state } = this.props;
    let currentComponent = state.path && [].concat(children).filter(child => { return child; }).filter(child => {
      return (child.props.route || state.path).toString().indexOf('(') > -1 ? state.path.match(child.props.route) : (child.props.route || state.path) == state.path && JSON.stringify(child.props.query || state.query) == JSON.stringify(state.query);
    })[0];

    if (currentComponent) {
      const matches = currentComponent.props.route.toString().indexOf('(') ? state.path.match(currentComponent.props.route) : [];
      currentComponent = React.cloneElement(currentComponent, { matches });
    } else { alert('Not found 404'); }

    return currentComponent || <div>no routes</div>;
  }
}
