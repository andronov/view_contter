import * as React from 'react';
import { observer, inject } from 'mobx-react';
import './style.scss';

@inject('appState')
@observer
export default class MainPageScreen extends React.Component {

  state = {
    canvas: null,
    context: null
  };

  componentDidMount() {
  }

  onLoadCanvas = (e) => {
    console.log('onLoadCanvas', e);
  };

  render() {

    return (
      <div style={{marginTop: '10px', marginLeft: '10px',
        height: '100%', overflow: 'auto'}} className=''>
        MainPageScreen

      </div>
    );
  }
}