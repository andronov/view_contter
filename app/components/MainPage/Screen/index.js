import * as React from 'react';
import './style.scss';

// Components
import './style.scss';

export default class Screen extends React.Component {


  render() {
  	const { loading, header, children, className } = this.props;

    return (
      <div className={`screen ${className || ''}`}>
        {header ?
          <div className='screen__header'>
            {header}
          </div>
          :
          null}
        <div className='screen__content'>
          {children}
        </div>
      </div>
    );
  }
}
