# Сontinuous integration web chat

[![N|Solid](https://crunchbase-production-res.cloudinary.com/image/upload/c_limit,h_600,w_600/v1436008976/xochjxbgsmpcuzs2wwtd.png)](https://gitlab.com/vsevolodg/web_chat)


На локальном сервере после изменений в своей ветки.
```sh
$ git checkout prod
$ git pull
$ git merge (со своей веткой)
$ git push
```
Переходим в репозиторий во вкладку pipelines https://gitlab.com/vsevolodg/web_chat/pipelines.
Спустя некоторое время появится коммит. Статус будет created, pending or runnig.

В случае успеха статус сменится на passed.
В случае ошибки статус сменится на failed. Нажав на него, можно открыть консоль и увидеть логи.